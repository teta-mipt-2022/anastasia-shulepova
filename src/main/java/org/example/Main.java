package org.example;
import org.example.Message.EnrichmentType;

public class Main {

    public static void main(String[] args) {
        ListOfUsers map = new ListOfUsers();
        Message message = new Message();
        EnrichmentService enrichmentService = new EnrichmentService();
        map.addUser("81234567890", new User("Гарри", "Поттер"));
        map.addUser("82123456789", new User("Гермиона", "Грейнджер"));
        map.addUser("83212345678", new User("Рон", "Уизли"));
        map.addUser("84321234567", new User("Невилл", "Долгопупс"));
        map.addUser("85432123456", new User("Северус", "Снейп"));
        map.addUser("86543212345", new User("Альбус", "Дамболдор"));
        map.addUser("87654321234", new User("Минерва", "Макгонагал"));
        map.addUser("88765432123", new User("Сириус", "Блэк"));
        map.addUser("89876543212", new User("Полумна", "Лавгуд"));
        map.addUser("80987654321", new User("Драко", "Малфой"));
        String exampleJSON = "{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"84321234567\",\"enrichment\":{\"firstname\":\"Невилл\",\"lastname\":\"Долгопупс\"}}";
        message.setContent(exampleJSON);
        message.setEnrichmentType(EnrichmentType.MSISDN);
        System.out.println(enrichmentService.enrich(message));
        System.out.println(enrichmentService.enrichedList.toString());
    }
}