package org.example;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.HashMap;

@Data
@AllArgsConstructor
public class User {
    String firstName;
    String lastName;

    public HashMap doMap(){
        HashMap<String,String> map =new HashMap<>();
        map.put("firstname",firstName);
        map.put("lastname",lastName);
        return map;
    }
}
