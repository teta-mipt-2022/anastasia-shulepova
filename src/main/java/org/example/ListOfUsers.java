package org.example;

import java.util.concurrent.ConcurrentHashMap;

public class ListOfUsers {
    static public ConcurrentHashMap<String, User> concurrentHashMap;
    public ListOfUsers() {
        concurrentHashMap = new ConcurrentHashMap<>();
    }
    public void addUser(String msisdn, User user){
        concurrentHashMap.put(msisdn,user);
    }
}