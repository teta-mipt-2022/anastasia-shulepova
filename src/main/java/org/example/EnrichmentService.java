package org.example;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class EnrichmentService {
    public ConcurrentHashMap<Integer, Message> enrichedList;
    public ConcurrentHashMap<Integer, Message> unenrichedlist;

    public EnrichmentService() {
        enrichedList = new ConcurrentHashMap<>();
        unenrichedlist = new ConcurrentHashMap<>();
    }

    public Message enrich(Message message) {
        if (message.getEnrichmentType() == Message.EnrichmentType.MSISDN) {
            return enrichByMSISDN(message);
        }
        unenrichedlist.put(unenrichedlist.size(),message);
        return message;
    }
    public  String getMSISDN(String content){
        String msisdn = null;
        if(isJSON(content)){
            Gson gson = new Gson();
            Map<String,String> map = gson.fromJson(content ,Map.class);
            if (map.containsKey("msisdn")) {
                msisdn = map.get("msisdn");
            }
        }
        return msisdn;
    }
    public boolean isJSON(String content) {
        try {
            JsonParser.parseString(content);
        } catch (JsonSyntaxException e) {
            return false;
        }
        return true;

    }
    public Message enrichByMSISDN(Message message){
        String msisdn=getMSISDN(message.getContent());
        User user;
        if(!Objects.equals(msisdn, null) && ListOfUsers.concurrentHashMap.containsKey(msisdn)){
            user = ListOfUsers.concurrentHashMap.get(msisdn);
            Gson gson = new Gson();
            Map<String,Object> map = gson.fromJson(message.getContent(),Map.class);
            map.put("enrichment",user.doMap());
            String json = gson.toJson(map);
            message.setContent(json);
            enrichedList.put(enrichedList.size(),message);
        } else {
            unenrichedlist.put(unenrichedlist.size(),message);
        } return message;
    }
}