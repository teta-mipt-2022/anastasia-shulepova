package org.example;

import org.junit.Test;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.Assert.assertEquals;

public class MainTest {
    @Test
    public void end2end() throws ExecutionException, InterruptedException {
        ListOfUsers map = new ListOfUsers();
        EnrichmentService enrichmentService= new EnrichmentService();
        Message message1 = new Message();
        Message message2 = new Message();
        Message message3 = new Message();
        Message message4 = new Message();
        Message message5 = new Message();
        String exampleJSON1="{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"81234567890\"}";
        String exampleJSON2="{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"82123456789\"}";
        String exampleJSON3="{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"84321234567\",\"enrichment\":{\"firstname\":\"Невилл\",\"lastname\":\"Долгопупс\"}}";
        String exampleJSON4="{\"page\":\"book_card\",\"msisdn\":\"88765432123\",\"enrichment\":{\"firstname\":\"Невилл\",\"lastname\":\"Долгопупс\"}}";
        String exampleJSON5="{}";
        map.addUser("81234567890",new User("Гарри","Поттер"));
        map.addUser("82123456789",new User("Гермиона","Грейнджер"));
        map.addUser("83212345678",new User("Рон","Уизли"));
        map.addUser("84321234567",new User("Невилл","Долгопупс"));
        map.addUser("85432123456",new User("Северус","Снейп"));
        map.addUser("86543212345",new User("Альбус","Дамболдор"));
        map.addUser("87654321234",new User("Минерва","Макгонагал"));
        map.addUser("88765432123",new User("Сириус","Блэк"));
        map.addUser("89876543212",new User("Полумна","Лавгуд"));
        map.addUser("80987654321",new User("Драко","Малфой"));
        message1.setContent(exampleJSON1);
        message1.setEnrichmentType(Message.EnrichmentType.MSISDN);
        message2.setContent(exampleJSON2);
        message2.setEnrichmentType(Message.EnrichmentType.MSISDN);
        message3.setContent(exampleJSON3);
        message3.setEnrichmentType(Message.EnrichmentType.MSISDN);
        message4.setContent(exampleJSON4);
        message4.setEnrichmentType(Message.EnrichmentType.MSISDN);
        message5.setContent(exampleJSON5);
        message5.setEnrichmentType(Message.EnrichmentType.MSISDN);
        ExecutorService executor = Executors.newFixedThreadPool(10);
        Future<String> future1 = executor.submit(() -> enrichmentService.enrich(message1).getContent());
        Future<String> future2 = executor.submit(() -> enrichmentService.enrich(message2).getContent());
        Future<String> future3 = executor.submit(() -> enrichmentService.enrich(message3).getContent());
        Future<String> future4 = executor.submit(() -> enrichmentService.enrich(message4).getContent());
        Future<String> future5 = executor.submit(() -> enrichmentService.enrich(message5).getContent());
        String response1 = future1.get();
        String response2 = future2.get();
        String response3 = future3.get();
        String response4 = future4.get();
        String response5 = future5.get();
        String exampleJSONresponce1="{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"81234567890\",\"enrichment\":{\"firstname\":\"Гарри\",\"lastname\":\"Поттер\"}}";
        String exampleJSONresponce2="{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"82123456789\",\"enrichment\":{\"firstname\":\"Гермиона\",\"lastname\":\"Грейнджер\"}}";
        String exampleJSONresponce3="{\"action\":\"button_click\",\"page\":\"book_card\",\"msisdn\":\"84321234567\",\"enrichment\":{\"firstname\":\"Невилл\",\"lastname\":\"Долгопупс\"}}";
        String exampleJSONresponce4="{\"page\":\"book_card\",\"msisdn\":\"88765432123\",\"enrichment\":{\"firstname\":\"Сириус\",\"lastname\":\"Блэк\"}}";;
        String exampleJSONresponce5="{}";
        assertEquals(exampleJSONresponce1,response1);
        assertEquals(exampleJSONresponce2,response2);
        assertEquals(exampleJSONresponce3,response3);
        assertEquals(exampleJSONresponce4,response4);
        assertEquals(exampleJSONresponce5,response5);
    }
}